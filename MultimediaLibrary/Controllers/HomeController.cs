﻿using Microsoft.AspNetCore.Mvc;
using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILibraryItemsService _libraryItemsService;

        public HomeController(ILibraryItemsService libraryItemsService)
        {
            _libraryItemsService = libraryItemsService;
        }

        public async Task<ActionResult<List<LibraryItem>>> Index()
        {
            var items = await _libraryItemsService.GetAllLibraryItemsAsync();
            return View(items);
        }
    }
}
