﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using MultimediaLibrary.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MultimediaLibrary.Controllers
{
    [Authorize]
    public class LibraryItemsController : Controller
    {
        private readonly ILibraryItemsService _libraryItemsService;
        private readonly ILibraryTypeService _libraryTypeService;
        private readonly IAuthorsService _authorsService;
        private readonly IPublishCompaniesService _publishCompaniesService;
        private readonly ILibraryItemContentsService _libraryItemContentsService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public LibraryItemsController(ILibraryItemsService libraryItemsService, ILibraryTypeService libraryTypeService, IAuthorsService authorsService, IPublishCompaniesService publishCompaniesService, IWebHostEnvironment webHostEnvironment, ILibraryItemContentsService libraryItemContentsService)
        {
            _libraryItemsService = libraryItemsService;
            _libraryTypeService = libraryTypeService;
            _authorsService = authorsService;
            _publishCompaniesService = publishCompaniesService;
            _webHostEnvironment = webHostEnvironment;
            _libraryItemContentsService = libraryItemContentsService;
        }

        public async Task<ActionResult<LibraryItem>> Index(string itemId = null, string displayMode = "thumbnails")
        {
            ViewBag.ViewMode = displayMode;
            var items = await _libraryItemsService.GetAllLibraryItemsAsync();

            if (string.IsNullOrEmpty(itemId)) return View(items);
            var orderedItems = OrderItemsFromList(itemId, items);

            return View(orderedItems);
        }

        private List<LibraryItem> OrderItemsFromList(string itemId, List<LibraryItem> items)
        {
            var orderedItems = new List<LibraryItem>();
            switch (itemId)
            {
                case "type":
                    orderedItems = items.OrderBy(i => i.LibraryType.LibraryTypeName).ToList();
                    break;
                case "author":
                    orderedItems = items.OrderBy(i => i.Author.LastName).ToList();
                    break;
                case "tag":
                    orderedItems = items.OrderBy(i => i.Tags).ToList();
                    break;
                case "publisher":
                    orderedItems = items.OrderBy(i => i.PublishCompany.Name).ToList();
                    break;
            }

            return orderedItems;
        }

        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<LibraryItemViewModel>> Add()
        {
            var viewModel = await GetLibraryItemViewModel();

            return View(viewModel);
        }
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<ActionResult<LibraryItemViewModel>> Add([FromForm] LibraryItemViewModel item)
        {

            if (item.LibraryTypeId != 3)
            {
                var result = ValidateIsbn(item);
                if (!result)
                    ModelState.AddModelError("ISBN", "Invalid ISBN Number.");
            }



            if (ModelState.IsValid)
            {
                var path = UploadedFile(item);
                var newItem = new LibraryItem
                {
                    AuthorId = item.AuthorId,
                    LibraryTypeId = item.LibraryTypeId,
                    FullName = item.FullName,
                    Description = item.Description,
                    ImageUrl = System.IO.Path.GetFileName(path),
                    PublishCompanyId = item.PublishCompanyId,
                    Tags = item.Tags
                };

                if (newItem.LibraryTypeId == 3)
                {
                    newItem.Length = TimeSpan.Parse(item.Length);

                }
                else
                {
                    newItem.Isbn = item.Isbn;
                    newItem.NumberOfPages = item.NumberOfPages;
                }

                if (_libraryItemsService.GetLibraryItemIsbnIdAsync(item.Isbn) != null)
                {
                    ModelState.AddModelError("ISBN", "Item with given ISBN exists.");
                    return View("Add", item);
                }

                await _libraryItemsService.AddLibraryItemAsync(newItem);
                var items = await _libraryItemsService.GetAllLibraryItemsAsync();
                var itemId = items.FirstOrDefault(x => x.FullName.Equals(item.FullName) && x.AuthorId == item.AuthorId && x.Isbn == item.Isbn).LibraryItemId;


                for (var i = 0; i <= Request.Form.Count; i++)
                {
                    var oridinalNumber = Request.Form["OridinalNumber[" + i + "]"];
                    var itemContent = Request.Form["ItemContent[" + i + "]"];

                    if (string.IsNullOrEmpty(oridinalNumber) || string.IsNullOrEmpty(itemContent)) continue;
                    var itemDetail = new LibraryItemContent
                    {
                        LibraryItemId = itemId,
                        ItemContent = itemContent,
                        OrdinalNumber = int.Parse(oridinalNumber)
                    };
                    await _libraryItemContentsService.AddLibraryItemContentAsync(itemDetail);
                }

                return RedirectToAction("Index");
            }

            var viewModel = await GetLibraryItemViewModel();
            item.Authors = viewModel.Authors;
            item.PublishCompanies = viewModel.PublishCompanies;
            item.LibraryTypes = viewModel.LibraryTypes;

            return View("Add", item);
        }

        public bool ValidateIsbn(LibraryItemBaseViewModel item)
        {
            var isbnRegex = new Regex(@"(ISBN[-]*(1[03])*[ ]*(: ){0,1})*(([0-9Xx][- ]*){13}|([0-9Xx][- ]*){10})");
            var result = item.Isbn != null && isbnRegex.IsMatch(item.Isbn);
            return result;
        }

        public async Task<ActionResult<FullLibraryItemViewModel>> Detail(int itemId)
        {
            var item = await _libraryItemsService.GetLibraryItemByIdAsync(itemId);
            var content = await _libraryItemContentsService.GetAllLibraryItemsContentByItemIdAsync(item.LibraryItemId);
            var orderedContent = content.OrderBy(x => x.OrdinalNumber);
            var viewModel = new FullLibraryItemViewModel
            {
                LibraryItem = item,
                LibraryItemContents = orderedContent
            };
            return View(viewModel);
        }
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<LibraryItemEditViewModel>> Edit(int itemId)
        {
            var dropdownsViewModel = await GetLibraryItemViewModel();
            var libraryItem = await _libraryItemsService.GetLibraryItemByIdAsync(itemId);
            var libraryItemContents = await _libraryItemContentsService.GetAllLibraryItemsContentByItemIdAsync(itemId);
            var libraryItemEditViewModel = new LibraryItemEditViewModel
            {
                LibraryTypes = dropdownsViewModel.LibraryTypes,
                Authors = dropdownsViewModel.Authors,
                PublishCompanies = dropdownsViewModel.PublishCompanies,
                AuthorId = libraryItem.AuthorId,
                Description = libraryItem.Description,
                FullName = libraryItem.FullName,
                LibraryItemContents = libraryItemContents,
                LibraryTypeId = libraryItem.LibraryTypeId,
                PublishCompanyId = libraryItem.PublishCompanyId,
                LibraryItemId = itemId,
                Tags = libraryItem.Tags,
            };


            if (libraryItem.LibraryTypeId != 3)
            {
                dropdownsViewModel.Isbn = libraryItem.Isbn;
                dropdownsViewModel.NumberOfPages = libraryItem.NumberOfPages;
            }
            else
            {
                dropdownsViewModel.Length = libraryItem.Length.ToString();
            }

            return View(libraryItemEditViewModel);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Edit(int itemId, [FromForm] LibraryItemEditViewModel item)
        {
            var oldItem = await _libraryItemsService.GetLibraryItemByIdAsync(itemId);
            if (item.LibraryTypeId != 3)
            {
                var result = ValidateIsbn(item);
                if (!result)
                    ModelState.AddModelError("ISBN", "Invalid ISBN Number.");
            }
            if (ModelState.IsValid || ModelState.Root.ValidationState == ModelValidationState.Unvalidated)
            {
                var newItem = new LibraryItem
                {
                    LibraryItemId = itemId,
                    AuthorId = item.AuthorId,
                    LibraryTypeId = item.LibraryTypeId,
                    FullName = item.FullName,
                    Description = item.Description,
                    ImageUrl = oldItem.ImageUrl,
                    PublishCompanyId = item.PublishCompanyId,
                    Tags = item.Tags
                };

                if (newItem.LibraryTypeId == 3)
                {
                    newItem.Length = TimeSpan.Parse(item.Length);

                }
                else
                {
                    newItem.Isbn = item.Isbn;
                    newItem.NumberOfPages = item.NumberOfPages;
                }

                await _libraryItemsService.UpdateLibraryItemAsync(newItem);

                var updatedItems = new List<LibraryItemContent>();
                for (var i = 0; i <= Request.Form.Count; i++)
                {
                    var id = Request.Form["IdNumber[" + i + "]"];
                    var oridinalNumber = Request.Form["OridinalNumber[" + i + "]"];
                    var itemContent = Request.Form["ItemContent[" + i + "]"];

                    if (string.IsNullOrEmpty(oridinalNumber) || string.IsNullOrEmpty(itemContent)) continue;

                    var itemDetail = new LibraryItemContent
                    {
                        LibraryItemId = itemId,
                        ItemContent = itemContent,
                        OrdinalNumber = int.Parse(oridinalNumber)
                    };
                    if (int.TryParse(id, out var newItemId))
                    {
                        itemDetail.LibraryItemContentId = newItemId;
                        await _libraryItemContentsService.UpdateLibraryItemContentAsync(itemDetail);
                    }
                    else
                    {
                        itemDetail.LibraryItemContentId = _libraryItemContentsService.AddLibraryItemContentAsync(itemDetail).Result.LibraryItemContentId;
                    }
                    updatedItems.Add(itemDetail);

                }

                var oldLibraryItemContents = _libraryItemContentsService.GetAllLibraryItemsContentByItemIdAsync(itemId).Result;

                var contentItemsToDelete = oldLibraryItemContents.Where(oldLibraryItemContent => !updatedItems.Exists(p => p.LibraryItemContentId == oldLibraryItemContent.LibraryItemContentId)).ToList();

                foreach (var libraryItemContent in contentItemsToDelete)
                {
                    _libraryItemContentsService.DeleteLibraryItemContent(libraryItemContent);
                }

                return RedirectToAction("Index");
            }


            var viewModel = await GetLibraryItemViewModel();
            item.Authors = viewModel.Authors;
            item.PublishCompanies = viewModel.PublishCompanies;
            item.LibraryTypes = viewModel.LibraryTypes;

            foreach (var modelStateItem in ModelState.Where(modelStateItem => modelStateItem.Value.ValidationState == ModelValidationState.Unvalidated))
            {
                ModelState.AddModelError(modelStateItem.Key, "Value has not been updated.");
            }

            return RedirectToAction("Edit", new { itemId });
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int itemId)
        {
            var itemsContent = _libraryItemContentsService.GetAllLibraryItemsContentByItemIdAsync(itemId).Result;

            foreach (var itemContent in itemsContent)
            {
                _libraryItemContentsService.DeleteLibraryItemContent(itemContent);
            }

            var item = _libraryItemsService.GetLibraryItemByIdAsync(itemId).Result;

            _libraryItemsService.DeleteLibraryItem(item);

            return RedirectToAction("Index");
        }


        [HttpPost]
        public async Task<ActionResult> Search()
        {
            var searchModel = new SearchLibraryItemViewModel
            {
                FullName = Request.Form["FullName"],
                Isbn = Request.Form["Isbn"],
                LastName = Request.Form["LastName"],
                Tags = Request.Form["Tags"],
                Publisher = Request.Form["Publisher"]
            };

            var items = await _libraryItemsService.GetAllLibraryItemsAsync();
            var foundItems = new HashSet<LibraryItem>();
            foreach (var item in items.Where(item => !string.IsNullOrEmpty(searchModel.Isbn)).Where(item => item.Isbn == searchModel.Isbn))
            {
                foundItems.Add(item);
            }

            foreach (var item in items.Where(item => !string.IsNullOrEmpty(searchModel.FullName)).Where(item => item.FullName == searchModel.FullName))
            {
                foundItems.Add(item);
            }

            foreach (var item in items.Where(item => !string.IsNullOrEmpty(searchModel.LastName)).Where(item => item.Author.LastName == searchModel.LastName))
            {
                foundItems.Add(item);
            }

            foreach (var item in items.Where(item => !string.IsNullOrEmpty(searchModel.Tags) && !string.IsNullOrEmpty(item.Tags)).Where(item => item.Tags.Contains(searchModel.Tags)))
            {
                foundItems.Add(item);
            }

            foreach (var item in items.Where(item => !string.IsNullOrEmpty(searchModel.Publisher)).Where(item => item.PublishCompany.Name == searchModel.Publisher))
            {
                foundItems.Add(item);
            }
            return View("Index", foundItems.ToImmutableList());
        }

        #region Private methods
        private async Task<LibraryItemViewModel> GetLibraryItemViewModel()
        {
            var authors = await _authorsService.GetAllAuthorsAsync();
            var libraryTypes = await _libraryTypeService.GetAllLibraryTypesAsync();
            var publishers = await _publishCompaniesService.GetAllPublishCompaniesAsync();

            var libraryTypesListItems = libraryTypes.Select(libraryType =>
                new SelectListItem($"{libraryType.LibraryTypeName}", libraryType.LibraryTypeId.ToString())).ToList();

            var authorsListItems = authors.Select(author =>
                new SelectListItem($"{author.FirstName} {author.LastName}", author.AuthorId.ToString())).ToList();
            var items = publishers
                .Select(publisher => new SelectListItem($"{publisher.Name}", publisher.PublishCompanyId.ToString())).ToList();
            var viewModel = new LibraryItemViewModel
            {
                LibraryTypes = libraryTypesListItems,
                PublishCompanies = items,
                Authors = authorsListItems
            };
            return viewModel;
        }

        private string UploadedFile(LibraryItemViewModel model)
        {
            var uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
            var uniqueFileName = model.CoverImage.FileName;
            var filePath = Path.Combine(uploadsFolder, uniqueFileName);
            using var fileStream = new FileStream(filePath, FileMode.Create);

            model.CoverImage.CopyTo(fileStream);

            return uniqueFileName;
        }

        private FormFile GetImageFile(string fileName)
        {
            var uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
            var filePath = Path.Combine(uploadsFolder, fileName);
            using var fileStream = new FileStream(filePath, FileMode.Open);
            var file = new FormFile(fileStream, fileStream.Length, fileStream.Length, fileStream.Name, fileName);
            return file;
        }

        #endregion




    }
}
