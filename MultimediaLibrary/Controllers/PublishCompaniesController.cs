﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MultimediaLibrary.Models;
using MultimediaLibrary.Models.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace MultimediaLibrary.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PublishCompaniesController : Controller
    {
        private readonly LibraryDbContext _context;

        public PublishCompaniesController(LibraryDbContext context)
        {
            _context = context;
        }

        // GET: PublishCompanies
        public async Task<IActionResult> Index()
        {
            return View(await _context.PublishCompanies.ToListAsync());
        }

        // GET: PublishCompanies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publishCompany = await _context.PublishCompanies
                .FirstOrDefaultAsync(m => m.PublishCompanyId == id);
            if (publishCompany == null)
            {
                return NotFound();
            }

            return View(publishCompany);
        }

        // GET: PublishCompanies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PublishCompanies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PublishCompanyId,Name")] PublishCompany publishCompany)
        {
            if (ModelState.IsValid)
            {
                _context.Add(publishCompany);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(publishCompany);
        }

        // GET: PublishCompanies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publishCompany = await _context.PublishCompanies.FindAsync(id);
            if (publishCompany == null)
            {
                return NotFound();
            }
            return View(publishCompany);
        }

        // POST: PublishCompanies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PublishCompanyId,Name")] PublishCompany publishCompany)
        {
            if (id != publishCompany.PublishCompanyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(publishCompany);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PublishCompanyExists(publishCompany.PublishCompanyId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(publishCompany);
        }

        // GET: PublishCompanies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publishCompany = await _context.PublishCompanies
                .FirstOrDefaultAsync(m => m.PublishCompanyId == id);
            if (publishCompany == null)
            {
                return NotFound();
            }

            return View(publishCompany);
        }

        // POST: PublishCompanies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var publishCompany = await _context.PublishCompanies.FindAsync(id);
            _context.PublishCompanies.Remove(publishCompany);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PublishCompanyExists(int id)
        {
            return _context.PublishCompanies.Any(e => e.PublishCompanyId == id);
        }
    }
}
