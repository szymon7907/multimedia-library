﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MultimediaLibrary.Migrations
{
    public partial class InitialSetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    AuthorId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.AuthorId);
                });

            migrationBuilder.CreateTable(
                name: "LibraryTypes",
                columns: table => new
                {
                    LibraryTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LibraryTypeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryTypes", x => x.LibraryTypeId);
                });

            migrationBuilder.CreateTable(
                name: "PublishCompanies",
                columns: table => new
                {
                    PublishCompanyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublishCompanies", x => x.PublishCompanyId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LibraryItems",
                columns: table => new
                {
                    LibraryItemId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LibraryTypeId = table.Column<int>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false),
                    PublishCompanyId = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    NumberOfPages = table.Column<int>(nullable: true),
                    Length = table.Column<TimeSpan>(nullable: true),
                    Isbn = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryItems", x => x.LibraryItemId);
                    table.ForeignKey(
                        name: "FK_LibraryItems_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "AuthorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LibraryItems_LibraryTypes_LibraryTypeId",
                        column: x => x.LibraryTypeId,
                        principalTable: "LibraryTypes",
                        principalColumn: "LibraryTypeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LibraryItems_PublishCompanies_PublishCompanyId",
                        column: x => x.PublishCompanyId,
                        principalTable: "PublishCompanies",
                        principalColumn: "PublishCompanyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LibraryItemContents",
                columns: table => new
                {
                    LibraryItemContentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LibraryItemId = table.Column<int>(nullable: false),
                    ItemContent = table.Column<string>(nullable: true),
                    OrdinalNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LibraryItemContents", x => x.LibraryItemContentId);
                    table.ForeignKey(
                        name: "FK_LibraryItemContents_LibraryItems_LibraryItemId",
                        column: x => x.LibraryItemId,
                        principalTable: "LibraryItems",
                        principalColumn: "LibraryItemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "107ca5f4-5f08-4ddf-aee9-e9fa0a87dfa0", "107ca5f4-5f08-4ddf-aee9-e9fa0a87dfa0", "Administrator", "ADMINISTRATOR" },
                    { "85b67184-cbcb-42cf-b963-1d1f63fda3cd", "85b67184-cbcb-42cf-b963-1d1f63fda3cd", "Viewer", "VIEWER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "006260d5-a79c-41e8-be14-6db29b9d0b84", 0, "46a8a06d-15fd-4ff9-a7e8-f7108d4780dd", "piotrgrad@gmail.com", true, false, null, "PIOTRGRAD@GMAIL.COM", "PIOTRGRAD@GMAIL.COM", "AQAAAAEAACcQAAAAEClV1FIcCwzgiB2Q1CI5PdAwMsKhDhlfu/vHCIGyVdon0Zknjw1NTkXdnUnl9rcdTw==", null, false, "c747dbaa-a562-4974-88c2-a24587af246c", false, "piotrgrad@gmail.com" },
                    { "00617509-9c63-4fe9-b1e1-b3de1d36c694", 0, "ae92f588-23e8-4737-a592-76a0795e8099", "appviewer@gmail.com", true, false, null, "APPVIEWER@GMAIL.COM", "APPVIEWER@GMAIL.COM", "AQAAAAEAACcQAAAAECs/JcrycXDOXEeD/t6K3ZdcoVD4X63ZW5LQNV/Spia3h5Ou2p2J81hFDshqcTeTdA==", null, false, "cd181386-3aa8-4ac3-97da-cb2021e28996", false, "appviewer@gmail.com" }
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "AuthorId", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 1, "Henryk", "Sienkiewicz" },
                    { 2, "Juliusz", "Verne" },
                    { 3, "Piotr", "Czajkowski" },
                    { 4, "Kuba", "Sienkiewicz" },
                    { 5, "Krzysztof", "Skibinski" }
                });

            migrationBuilder.InsertData(
                table: "LibraryTypes",
                columns: new[] { "LibraryTypeId", "LibraryTypeName" },
                values: new object[,]
                {
                    { 1, "Books" },
                    { 2, "Magazines" },
                    { 3, "Multimedia" }
                });

            migrationBuilder.InsertData(
                table: "PublishCompanies",
                columns: new[] { "PublishCompanyId", "Name" },
                values: new object[,]
                {
                    { 1, "Polskie Nagrania" },
                    { 2, "Helion" },
                    { 3, "Poznańskie Słowiki" },
                    { 4, "Oko Press" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { "006260d5-a79c-41e8-be14-6db29b9d0b84", "107ca5f4-5f08-4ddf-aee9-e9fa0a87dfa0" },
                    { "00617509-9c63-4fe9-b1e1-b3de1d36c694", "85b67184-cbcb-42cf-b963-1d1f63fda3cd" }
                });

            migrationBuilder.InsertData(
                table: "LibraryItems",
                columns: new[] { "LibraryItemId", "AuthorId", "Description", "FullName", "ImageUrl", "Isbn", "Length", "LibraryTypeId", "NumberOfPages", "PublishCompanyId", "Tags" },
                values: new object[,]
                {
                    { 3, 3, "Najlepsze nagrania rockowe, Kraftwerk", "Awangarda Rocka", "kraftwerk.jpg", null, new TimeSpan(0, 1, 13, 25, 0), 3, null, 1, "Rock around the clock, Kraftwerk" },
                    { 1, 1, "Opowieść o perypetiach pierwszy chrześcijan.", "Quo Vadis", "quo-vadis.jpg", "1234123412341", null, 1, 425, 2, "Sienkiewicz, Chrześcijanie, Rzym, Neron" },
                    { 2, 1, "Ostatnia cześć trylogii.", "Pan Wołodyjowski", "pan-wolodyjowski.jpg", "9871234321123", null, 1, 368, 4, "Trylogia, Mały Rycerz" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_LibraryItemContents_LibraryItemId",
                table: "LibraryItemContents",
                column: "LibraryItemId");

            migrationBuilder.CreateIndex(
                name: "IX_LibraryItems_AuthorId",
                table: "LibraryItems",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_LibraryItems_LibraryTypeId",
                table: "LibraryItems",
                column: "LibraryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_LibraryItems_PublishCompanyId",
                table: "LibraryItems",
                column: "PublishCompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "LibraryItemContents");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "LibraryItems");

            migrationBuilder.DropTable(
                name: "Authors");

            migrationBuilder.DropTable(
                name: "LibraryTypes");

            migrationBuilder.DropTable(
                name: "PublishCompanies");
        }
    }
}
