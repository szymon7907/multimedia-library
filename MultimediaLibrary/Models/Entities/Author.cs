﻿using System.Collections.Generic;

namespace MultimediaLibrary.Models.Entities
{
    public class Author
    {
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<LibraryItem> LibraryItems { get; set; }

    }
}