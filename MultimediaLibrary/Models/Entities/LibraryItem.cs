﻿using System;
using System.Collections.Generic;

namespace MultimediaLibrary.Models.Entities
{
    public class LibraryItem
    {
        public int LibraryItemId { get; set; }
        public int LibraryTypeId { get; set; }
        public LibraryType LibraryType { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
        public int PublishCompanyId { get; set; }
        public PublishCompany PublishCompany { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int? NumberOfPages { get; set; }
        public TimeSpan? Length { get; set; }
        public string Isbn { get; set; }
        public string Tags { get; set; }
        public List<LibraryItemContent> Content { get; set; }

    }
}
