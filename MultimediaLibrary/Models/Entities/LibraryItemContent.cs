﻿namespace MultimediaLibrary.Models.Entities
{
    public class LibraryItemContent
    {
        public int LibraryItemContentId { get; set; }
        public int LibraryItemId { get; set; }
        public LibraryItem LibraryItem { get; set; }
        public string ItemContent { get; set; }
        public int OrdinalNumber { get; set; }
    }
}