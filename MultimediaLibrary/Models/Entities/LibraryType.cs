﻿using System.Collections.Generic;

namespace MultimediaLibrary.Models.Entities
{
    public class LibraryType
    {
        public int LibraryTypeId { get; set; }
        public string LibraryTypeName { get; set; }
        public List<LibraryItem> LibraryItems { get; set; }
    }
}