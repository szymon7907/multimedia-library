﻿using System.Collections.Generic;

namespace MultimediaLibrary.Models.Entities
{
    public class PublishCompany
    {
        public int PublishCompanyId { get; set; }
        public string Name { get; set; }
        public List<LibraryItem> LibraryItems { get; set; }
    }
}
