﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MultimediaLibrary.Models.Entities;
using System;
using static System.Guid;

namespace MultimediaLibrary.Models
{
    public class LibraryDbContext : IdentityDbContext<IdentityUser>
    {
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options) : base(options)
        {

        }
        public DbSet<LibraryItem> LibraryItems { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<PublishCompany> PublishCompanies { get; set; }
        public DbSet<LibraryType> LibraryTypes { get; set; }
        public DbSet<LibraryItemContent> LibraryItemContents { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.EnableSensitiveDataLogging();
        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            //seed categories
            modelBuilder.Entity<PublishCompany>().HasData(new PublishCompany { PublishCompanyId = 1, Name = "Polskie Nagrania" });
            modelBuilder.Entity<PublishCompany>().HasData(new PublishCompany { PublishCompanyId = 2, Name = "Helion" });
            modelBuilder.Entity<PublishCompany>().HasData(new PublishCompany { PublishCompanyId = 3, Name = "Poznańskie Słowiki" });
            modelBuilder.Entity<PublishCompany>().HasData(new PublishCompany { PublishCompanyId = 4, Name = "Oko Press" });

            modelBuilder.Entity<LibraryType>().HasData(new LibraryType { LibraryTypeId = 1, LibraryTypeName = "Books" });
            modelBuilder.Entity<LibraryType>().HasData(new LibraryType { LibraryTypeId = 2, LibraryTypeName = "Magazines" });
            modelBuilder.Entity<LibraryType>().HasData(new LibraryType { LibraryTypeId = 3, LibraryTypeName = "Multimedia" });

            modelBuilder.Entity<Author>().HasData(new Author { AuthorId = 1, FirstName = "Henryk", LastName = "Sienkiewicz" });
            modelBuilder.Entity<Author>().HasData(new Author { AuthorId = 2, FirstName = "Juliusz", LastName = "Verne" });
            modelBuilder.Entity<Author>().HasData(new Author { AuthorId = 3, FirstName = "Piotr", LastName = "Czajkowski" });
            modelBuilder.Entity<Author>().HasData(new Author { AuthorId = 4, FirstName = "Kuba", LastName = "Sienkiewicz" });
            modelBuilder.Entity<Author>().HasData(new Author { AuthorId = 5, FirstName = "Krzysztof", LastName = "Skibinski" });

            modelBuilder.Entity<LibraryItem>().HasData(new LibraryItem
            {
                LibraryItemId = 1,
                AuthorId = 1,
                FullName = "Quo Vadis",
                LibraryTypeId = 1,
                Description = "Opowieść o perypetiach pierwszy chrześcijan.",
                Isbn = "1234123412341",
                NumberOfPages = 425,
                PublishCompanyId = 2,
                ImageUrl = "quo-vadis.jpg",
                Tags = "Sienkiewicz, Chrześcijanie, Rzym, Neron"
            });
            modelBuilder.Entity<LibraryItem>().HasData(new LibraryItem
            {
                LibraryItemId = 2,
                AuthorId = 1,
                FullName = "Pan Wołodyjowski",
                LibraryTypeId = 1,
                Description = "Ostatnia cześć trylogii.",
                Isbn = "9871234321123",
                NumberOfPages = 368,
                PublishCompanyId = 4,
                ImageUrl = "pan-wolodyjowski.jpg",
                Tags = "Trylogia, Mały Rycerz"
            });

            modelBuilder.Entity<LibraryItem>().HasData(new LibraryItem
            {
                LibraryItemId = 3,
                AuthorId = 3,
                FullName = "Awangarda Rocka",
                LibraryTypeId = 3,
                Description = "Najlepsze nagrania rockowe, Kraftwerk",
                Length = new TimeSpan(0, 73, 25),
                PublishCompanyId = 1,
                ImageUrl = "kraftwerk.jpg",
                Tags = "Rock around the clock, Kraftwerk"
            });

            var adminId = NewGuid().ToString();
            var roleId = NewGuid().ToString();
            var viewerRoleId = NewGuid().ToString();
            var viewerId = NewGuid().ToString();
            //seed admin role
            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Name = "Administrator",
                NormalizedName = "Administrator".ToUpperInvariant(),
                Id = roleId,
                ConcurrencyStamp = roleId
            });
            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Name = "Viewer",
                NormalizedName = "Viewer".ToUpperInvariant(),
                Id = viewerRoleId,
                ConcurrencyStamp = viewerRoleId
            });
            //create user
            var appUser = new IdentityUser
            {
                Id = adminId,
                Email = "piotrgrad@gmail.com",
                EmailConfirmed = true,
                UserName = "piotrgrad@gmail.com",
                NormalizedEmail = "piotrgrad@gmail.com".ToUpperInvariant(),
                NormalizedUserName = "piotrgrad@gmail.com".ToUpperInvariant(),
            };
            var appViewerUser = new IdentityUser
            {
                Id = viewerId,
                Email = "appviewer@gmail.com",
                EmailConfirmed = true,
                UserName = "appviewer@gmail.com",
                NormalizedEmail = "appviewer@gmail.com".ToUpperInvariant(),
                NormalizedUserName = "appviewer@gmail.com".ToUpperInvariant(),
            };
            //set user password
            var ph = new PasswordHasher<IdentityUser>();
            appUser.PasswordHash = ph.HashPassword(appUser, "Alabama12#");
            appViewerUser.PasswordHash = ph.HashPassword(appViewerUser, "Jordania12#");
            //seed users
            modelBuilder.Entity<IdentityUser>().HasData(appUser);
            modelBuilder.Entity<IdentityUser>().HasData(appViewerUser);

            //set user role to admin,viewer
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = roleId,
                UserId = adminId
            });
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = viewerRoleId,
                UserId = viewerId
            });
        }

    }
}
