﻿using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services
{
    public class AuthorsService : IAuthorsService
    {
        private readonly IAuthorsRepository _authorsRepository;

        public AuthorsService(IAuthorsRepository authorsRepository)
        {
            _authorsRepository = authorsRepository;
        }

        public async Task<Author> AddAuthorAsync(Author author)
        {
            return await _authorsRepository.AddAsync(author);
        }

        public async Task<Author> GetAuthorByIdAsync(int id)
        {
            return await _authorsRepository.GetAuthorByIdAsync(id);
        }

        public async Task<Author> UpdateAuthorAsync(Author author)
        {
            return await _authorsRepository.UpdateAsync(author);
        }

        public async Task<List<Author>> GetAllAuthorsAsync()
        {
            return await _authorsRepository.GetAllAuthorsAsync();
        }
    }
}
