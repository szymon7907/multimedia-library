﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface IAuthorsRepository : IRepository<Author>
    {
        Task<Author> GetAuthorByIdAsync(int id);
        Task<List<Author>> GetAllAuthorsAsync();
    }
}
