﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface IAuthorsService
    {
        Task<Author> AddAuthorAsync(Author author);

        Task<Author> GetAuthorByIdAsync(int id);

        Task<Author> UpdateAuthorAsync(Author author);
        Task<List<Author>> GetAllAuthorsAsync();
    }
}
