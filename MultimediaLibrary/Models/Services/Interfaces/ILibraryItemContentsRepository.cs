﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface ILibraryItemContentsRepository : IRepository<LibraryItemContent>
    {
        Task<LibraryItemContent> GetLibraryItemContentByIdAsync(int id);
        Task<List<LibraryItemContent>> GetLibraryItemContentsByItemIdAsync(int itemId);
    }
}
