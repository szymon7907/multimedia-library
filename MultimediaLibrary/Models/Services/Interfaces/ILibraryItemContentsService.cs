﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface ILibraryItemContentsService
    {
        Task<LibraryItemContent> AddLibraryItemContentAsync(LibraryItemContent libraryItemContent);
        Task<LibraryItemContent> GetLibraryItemContentByIdAsync(int id);
        Task<LibraryItemContent> UpdateLibraryItemContentAsync(LibraryItemContent libraryItemContent);
        LibraryItemContent DeleteLibraryItemContent(LibraryItemContent libraryItemContent);
        Task<List<LibraryItemContent>> GetAllLibraryItemsContentByItemIdAsync(int itemId);
    }
}
