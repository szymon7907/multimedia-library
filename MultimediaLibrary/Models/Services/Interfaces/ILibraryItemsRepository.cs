﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface ILibraryItemsRepository : IRepository<LibraryItem>
    {
        Task<LibraryItem> GetLibraryItemIdAsync(int id);
        Task<LibraryItem> GetLibraryItemIsbnIdAsync(string id);
        Task<List<LibraryItem>> GetAllLibraryItemsAsync();
    }
}
