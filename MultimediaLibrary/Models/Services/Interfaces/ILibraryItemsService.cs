﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface ILibraryItemsService
    {
        Task<LibraryItem> AddLibraryItemAsync(LibraryItem libraryItem);

        Task<LibraryItem> GetLibraryItemByIdAsync(int id);
        Task<LibraryItem> GetLibraryItemIsbnIdAsync(string isbn);

        Task<LibraryItem> UpdateLibraryItemAsync(LibraryItem libraryItem);
        LibraryItem DeleteLibraryItem(LibraryItem libraryItem);
        Task<List<LibraryItem>> GetAllLibraryItemsAsync();


    }
}
