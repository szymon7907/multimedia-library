﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface ILibraryTypeRepository : IRepository<LibraryType>
    {
        Task<LibraryType> GetLibraryTypeIdAsync(int id);
        Task<List<LibraryType>> GetAllLibraryTypesAsync();
    }


}
