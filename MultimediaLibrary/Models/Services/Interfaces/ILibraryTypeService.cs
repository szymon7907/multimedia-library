﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface ILibraryTypeService
    {
        Task<LibraryType> AddLibraryTypeAsync(LibraryType libraryType);

        Task<LibraryType> GetLibraryTypeByIdAsync(int id);

        Task<LibraryType> UpdateLibraryTypeAsync(LibraryType libraryType);
        Task<List<LibraryType>> GetAllLibraryTypesAsync();
    }
}
