﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Interfaces
{
    public interface IPublishCompaniesRepository : IRepository<PublishCompany>
    {
        Task<List<PublishCompany>> GetAllPublicCompaniesAsync();
    }
}
