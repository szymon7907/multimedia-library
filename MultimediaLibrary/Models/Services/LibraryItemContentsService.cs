﻿using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services
{
    public class LibraryItemContentsService : ILibraryItemContentsService
    {
        private readonly ILibraryItemContentsRepository _libraryItemContentsRepository;

        public LibraryItemContentsService(ILibraryItemContentsRepository libraryItemContentsRepository)
        {
            _libraryItemContentsRepository = libraryItemContentsRepository;
        }

        public async Task<LibraryItemContent> AddLibraryItemContentAsync(LibraryItemContent libraryItemContent)
        {
            return await _libraryItemContentsRepository.AddAsync(libraryItemContent);
        }

        public async Task<LibraryItemContent> GetLibraryItemContentByIdAsync(int id)
        {
            return await _libraryItemContentsRepository.GetLibraryItemContentByIdAsync(id);
        }

        public async Task<LibraryItemContent> UpdateLibraryItemContentAsync(LibraryItemContent libraryItemContent)
        {
            return await _libraryItemContentsRepository.UpdateAsync(libraryItemContent);
        }

        public LibraryItemContent DeleteLibraryItemContent(LibraryItemContent libraryItemContent)
        {
            return _libraryItemContentsRepository.Delete(libraryItemContent);
        }

        public async Task<List<LibraryItemContent>> GetAllLibraryItemsContentByItemIdAsync(int itemId)
        {
            return await _libraryItemContentsRepository.GetLibraryItemContentsByItemIdAsync(itemId);
        }
    }
}
