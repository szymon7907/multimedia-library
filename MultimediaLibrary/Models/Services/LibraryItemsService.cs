﻿using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services
{
    public class LibraryItemsService : ILibraryItemsService
    {
        private readonly ILibraryItemsRepository _libraryItemsRepository;

        public LibraryItemsService(ILibraryItemsRepository libraryItemsRepository)
        {
            _libraryItemsRepository = libraryItemsRepository;
        }

        public async Task<LibraryItem> AddLibraryItemAsync(LibraryItem libraryItem)
        {
            return await _libraryItemsRepository.AddAsync(libraryItem);
        }

        public async Task<LibraryItem> GetLibraryItemByIdAsync(int id)
        {
            return await _libraryItemsRepository.GetLibraryItemIdAsync(id);
        }

        public async Task<LibraryItem> GetLibraryItemIsbnIdAsync(string isbn)
        {
            return await _libraryItemsRepository.GetLibraryItemIsbnIdAsync(isbn);
        }

        public async Task<LibraryItem> UpdateLibraryItemAsync(LibraryItem libraryItem)
        {
            return await _libraryItemsRepository.UpdateAsync(libraryItem);
        }

        public LibraryItem DeleteLibraryItem(LibraryItem libraryItem)
        {
            return _libraryItemsRepository.Delete(libraryItem);
        }

        public async Task<List<LibraryItem>> GetAllLibraryItemsAsync()
        {
            return await _libraryItemsRepository.GetAllLibraryItemsAsync();
        }
    }
}
