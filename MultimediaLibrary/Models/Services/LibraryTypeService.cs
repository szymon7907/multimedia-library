﻿using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services
{
    public class LibraryTypeService : ILibraryTypeService
    {
        private readonly ILibraryTypeRepository _libraryTypeRepository;

        public LibraryTypeService(ILibraryTypeRepository libraryTypeRepository)
        {
            _libraryTypeRepository = libraryTypeRepository;
        }

        public async Task<LibraryType> AddLibraryTypeAsync(LibraryType libraryType)
        {
            return await _libraryTypeRepository.AddAsync(libraryType);
        }

        public async Task<LibraryType> GetLibraryTypeByIdAsync(int id)
        {
            return await _libraryTypeRepository.GetLibraryTypeIdAsync(id);
        }

        public async Task<LibraryType> UpdateLibraryTypeAsync(LibraryType libraryType)
        {
            return await _libraryTypeRepository.UpdateAsync(libraryType);
        }

        public async Task<List<LibraryType>> GetAllLibraryTypesAsync()
        {
            return await _libraryTypeRepository.GetAllLibraryTypesAsync();
        }
    }
}
