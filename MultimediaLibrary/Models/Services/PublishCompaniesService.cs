﻿using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services
{
    public class PublishCompaniesService : IPublishCompaniesService
    {
        private readonly IPublishCompaniesRepository _publishCompaniesRepository;

        public PublishCompaniesService(IPublishCompaniesRepository publishCompaniesRepository)
        {
            _publishCompaniesRepository = publishCompaniesRepository;
        }

        public async Task<List<PublishCompany>> GetAllPublishCompaniesAsync()
        {
            return await _publishCompaniesRepository.GetAllPublicCompaniesAsync();
        }
    }
}
