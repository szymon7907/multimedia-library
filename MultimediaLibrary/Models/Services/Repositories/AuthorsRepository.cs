﻿using Microsoft.EntityFrameworkCore;
using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Repositories
{
    public class AuthorsRepository : Repository<Author>, IAuthorsRepository
    {
        public AuthorsRepository(LibraryDbContext context) : base(context)
        {
        }

        public Task<Author> GetAuthorByIdAsync(int id)
        {
            return GetAll().FirstOrDefaultAsync(x => x.AuthorId == id);
        }

        public Task<List<Author>> GetAllAuthorsAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}
