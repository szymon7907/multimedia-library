﻿using Microsoft.EntityFrameworkCore;
using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Repositories
{
    public class LibraryItemContentsRepository : Repository<LibraryItemContent>, ILibraryItemContentsRepository
    {
        public LibraryItemContentsRepository(LibraryDbContext context) : base(context)
        {
        }

        public Task<LibraryItemContent> GetLibraryItemContentByIdAsync(int id)
        {
            return GetAll().FirstOrDefaultAsync(x => x.LibraryItemContentId == id);
        }
        public Task<List<LibraryItemContent>> GetLibraryItemContentsByItemIdAsync(int itemId)
        {
            return GetAll()
                .Where(id => id.LibraryItem.LibraryItemId == itemId)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
