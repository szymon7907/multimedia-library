﻿using Microsoft.EntityFrameworkCore;
using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Repositories
{
    public class LibraryItemsRepository : Repository<LibraryItem>, ILibraryItemsRepository
    {
        public LibraryItemsRepository(LibraryDbContext context) : base(context)
        {

        }

        public Task<LibraryItem> GetLibraryItemIdAsync(int id)
        {
            return GetAll()
                .Include(a => a.Author)
                .Include(navigationPropertyPath: i => i.LibraryType)
                .Include(pc => pc.PublishCompany)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.LibraryItemId == id);
        }

        public Task<LibraryItem> GetLibraryItemIsbnIdAsync(string isbn)
        {
            return GetAll()
                .Include(a => a.Author)
                .Include(navigationPropertyPath: i => i.LibraryType)
                .Include(pc => pc.PublishCompany)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Isbn == isbn);
        }

        public Task<List<LibraryItem>> GetAllLibraryItemsAsync()
        {
            return GetAll()
                .Include(a => a.Author)
                .Include(navigationPropertyPath: i => i.LibraryType)
                .Include(pc => pc.PublishCompany)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
