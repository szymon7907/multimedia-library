﻿using Microsoft.EntityFrameworkCore;
using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Repositories
{
    public class LibraryTypeRepository : Repository<LibraryType>, ILibraryTypeRepository
    {
        public LibraryTypeRepository(LibraryDbContext context) : base(context)
        {
        }

        public Task<LibraryType> GetLibraryTypeIdAsync(int id)
        {
            return GetAll().FirstOrDefaultAsync(x => x.LibraryTypeId == id);
        }

        public Task<List<LibraryType>> GetAllLibraryTypesAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}
