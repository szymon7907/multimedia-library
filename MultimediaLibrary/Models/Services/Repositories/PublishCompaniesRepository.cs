﻿using Microsoft.EntityFrameworkCore;
using MultimediaLibrary.Models.Entities;
using MultimediaLibrary.Models.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultimediaLibrary.Models.Services.Repositories
{
    public class PublishCompaniesRepository : Repository<PublishCompany>, IPublishCompaniesRepository
    {
        public PublishCompaniesRepository(LibraryDbContext context) : base(context)
        {
        }

        public Task<List<PublishCompany>> GetAllPublicCompaniesAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}
