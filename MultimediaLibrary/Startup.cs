using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MultimediaLibrary.Models;
using MultimediaLibrary.Models.Services;
using MultimediaLibrary.Models.Services.Interfaces;
using MultimediaLibrary.Models.Services.Repositories;

namespace MultimediaLibrary
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<LibraryDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>().AddRoles<IdentityRole>().AddEntityFrameworkStores<LibraryDbContext>();

            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<ILibraryItemsRepository, LibraryItemsRepository>();
            services.AddTransient<IAuthorsRepository, AuthorsRepository>();
            services.AddTransient<ILibraryTypeRepository, LibraryTypeRepository>();
            services.AddTransient<IPublishCompaniesRepository, PublishCompaniesRepository>();
            services.AddTransient<ILibraryItemContentsRepository, LibraryItemContentsRepository>();

            services.AddTransient<ILibraryItemsService, LibraryItemsService>();
            services.AddTransient<IAuthorsService, AuthorsService>();
            services.AddTransient<ILibraryTypeService, LibraryTypeService>();
            services.AddTransient<IPublishCompaniesService, PublishCompaniesService>();
            services.AddTransient<ILibraryItemContentsService, LibraryItemContentsService>();

            services.AddControllersWithViews().AddMvcOptions(options =>
            {
                options.ModelBindingMessageProvider.SetValueMustNotBeNullAccessor(
                    _ => "This field is mandatory.");
            });
            services.AddRazorPages();
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("RequireAdministratorRole",
            //        policy => policy.RequireRole("Administrator"));
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "Authors",
                    pattern: "{controller=Authors}/{action=Delete}/{id?}");
                endpoints.MapControllerRoute(
                    name: "PublishCompanies",
                    pattern: "{controller=PublishCompanies}/{action=Delete}/{id?}");
                endpoints.MapControllerRoute(
                    name: "LibraryItemSelectDisplay",
                    pattern: "{controller=LibraryItem}/{action=Index}/{itemId?}");
                endpoints.MapControllerRoute(
                    name: "LibraryItemSelect",
                    pattern: "{controller=LibraryItem}/{action=Index}/{itemId?}/{displayMode?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Authors}/{action=Delete}/{id?}");

                endpoints.MapRazorPages();
            });
        }
    }
}
