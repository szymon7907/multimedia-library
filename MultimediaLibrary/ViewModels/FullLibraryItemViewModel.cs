﻿using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;

namespace MultimediaLibrary.ViewModels
{
    public class FullLibraryItemViewModel
    {
        public LibraryItem LibraryItem { get; set; }
        public IEnumerable<LibraryItemContent> LibraryItemContents { get; set; }
    }
}
