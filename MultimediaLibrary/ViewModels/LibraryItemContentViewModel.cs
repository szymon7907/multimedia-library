﻿using MultimediaLibrary.Models.Entities;

namespace MultimediaLibrary.ViewModels
{
    public class LibraryItemContentViewModel
    {
        public LibraryItem LibraryItem { get; set; }
        public string ItemContent { get; set; }
        public int OrdinalNumber { get; set; }
    }
}
