﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using MultimediaLibrary.Models.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MultimediaLibrary.ViewModels
{
    public class LibraryItemBaseViewModel
    {
        public List<SelectListItem> LibraryTypes { get; set; }
        public List<SelectListItem> Authors { get; set; }
        public List<SelectListItem> PublishCompanies { get; set; }

        [Display(Name = "Publisher")]
        [Required(ErrorMessage = "Publisher is required.")]
        public int PublishCompanyId { get; set; }
        [Required(ErrorMessage = "Author is required.")]
        [BindRequired]
        [Display(Name = "Author")]
        public int AuthorId { get; set; }
        [Required(ErrorMessage = "Resource is required.")]
        [Display(Name = "Resource")]
        public int LibraryTypeId { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required.")]
        public string FullName { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is mandatory.")]
        public string Description { get; set; }

        [Display(Name = "Number of pages")]
        public int? NumberOfPages { get; set; }
        [Display(Name = "Album Length")]
        public string Length { get; set; }
        [Display(Name = "ISBN")]
        public string Isbn { get; set; }
        [Display(Name = "Tags")]
        public string Tags { get; set; }
        public List<LibraryItemContent> LibraryItemContents { get; set; }
        public int LibraryItemId { get; set; }
    }

    public class LibraryItemViewModel : LibraryItemBaseViewModel
    {
        [Required(ErrorMessage = "Cover is mandatory.")]
        [Display(Name = "Cover")]
        public IFormFile CoverImage { get; set; }
    }

    public class LibraryItemEditViewModel : LibraryItemBaseViewModel
    {

    }
}
