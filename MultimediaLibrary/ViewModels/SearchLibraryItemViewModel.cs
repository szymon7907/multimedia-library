﻿namespace MultimediaLibrary.ViewModels
{
    public class SearchLibraryItemViewModel
    {
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string Tags { get; set; }
        public string Isbn { get; set; }
        public string Publisher { get; set; }


    }
}
